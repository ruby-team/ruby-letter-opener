require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:spec) do |spec|
  spec.pattern = './spec/letter_opener/message_spec.rb'
end

task :default => :spec
